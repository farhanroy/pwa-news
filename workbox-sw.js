importScripts('node-modules/workbox-sw.prod.v2.1.3.js');

const cacheFiles = [
    '.',
    '/index.html',
    '/js/index.js',
    '/css/bootstrap.min.css',
    '/favicon.ico',
    '/manifest.json'
];

const wb = new WorkboxSW();

wb.precache(cacheFiles);
wb.router.registerRoute('https://newsapi.org/(.*)', wb.strategies.networkFirst());
wb.router.registerRoute(/.*\.(png|jpg|jpeg|gif)/, wb.strategies.cacheFirst({
    cacheName : 'news',
    cacheExpiration : {maxEntries : 20, maxAgeSeconds : 12 * 60 * 60},
    cacheableResponse : {statuses : [0,200]}
}));