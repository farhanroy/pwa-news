let baris = document.querySelector('#main');

fetch('https://newsapi.org/v2/top-headlines?sources=bbc-sport&sortBy=top&apiKey=a441d239d9c1498497b7393e41d4db38')
.then(response => response.json())
.then(data => {
    console.log(data)
    baris.innerHTML = data.articles.map(elemenBaru)
})
elemenBaru = (article) => {
    return `
    <div class="card col-lg-4 mt-3">
      <img class="card-img-top" src="${article.urlToImage}" alt="Card image cap">
      <div class="card-body">
        <p class="card-text">${article.description}</p>
      </div>
    </div>
    `
}